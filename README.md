# IP Fabric Marketing Examples

## IP Fabric

[IP Fabric](https://ipfabric.io) is a vendor-neutral network assurance platform that automates the 
holistic discovery, verification, visualization, and documentation of 
large-scale enterprise networks, reducing the associated costs and required 
resources whilst improving security and efficiency.

It supports your engineering and operations teams, underpinning migration and 
transformation projects. IP Fabric will revolutionize how you approach network 
visibility and assurance, security assurance, automation, multi-cloud 
networking, and trouble resolution.

**Integrations or scripts should not be installed directly on the IP Fabric VM unless directly communicated from the
IP Fabric Support or Solution Architect teams.  Any action on the Command-Line Interface (CLI) using the root, osadmin,
or autoboss account may cause irreversible, detrimental changes to the product and can render the system unusable.**

## Project status

**Code located in this repository is not actively maintained and updated.**

Using examples in this directory may require some modifications in order to get
working properly. If you would like to use one of the examples but having 
difficulty running it or need assistance with updating it, please contact us
in one of the following ways:

- Reach out to the Blog Author or Video Presenter.
- Contact your Solution Architect.
- Open a [GitLab Issue](https://gitlab.com/ip-fabric/integrations/marketing-examples/-/issues)
