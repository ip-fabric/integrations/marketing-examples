# [Using IP Fabric’s API to add devices into NAGIOS XI](https://ipfabric.io/blog/ipfabric-nagios-api-integration/)

Author: [Sebastien d'Argoeuves](mailto:sebastien.dargoeuves@ipfabric.io)<br>
Published Date: 2021-08-30<br>
Updated Date: N/A

## Summary

In this blog post we are going to show you how we can use the API Clients created for IP Fabric and NAGIOS. We are going to work on a simple scenario where we want to create hosts discovered by IP Fabric into NAGIOS XI.

## Project status

**Code located in this repository is not actively maintained and updated.**

Using examples in this directory may require some modifications in order to get
working properly. If you would like to use one of the examples but having
difficulty running it or need assistance with updating it, please contact us
in one of the following ways:

- Reach out to the Blog Author or Video Presenter.
- Contact your Solution Architect.
- Open a [GitLab Issue](https://gitlab.com/ip-fabric/integrations/marketing-examples/-/issues)


