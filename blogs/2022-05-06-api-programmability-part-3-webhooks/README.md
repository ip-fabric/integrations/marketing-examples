# [API Programmability - Part 3: Webhooks](https://ipfabric.io/blog/api-programmability-part-3-webhooks/)

Author: [Justin Jeffery](mailto:justin.jeffery@ipfabric.io)<br>
Published Date: 2022-05-06<br>
Updated Date: N/A

## Summary

IP Fabric enables you to send webhooks to other systems.  What if your other system does not
accept webhooks or you are not able to program logic around incoming data?  In this post we will
create our own webhook listener using FastAPI.

Note: This will deploy using HTTP not HTTPS.  If you want to secure your webpage consider 
using Nginx or visit https://fastapi.tiangolo.com/deployment/https/

Note: Heavy background computation processes might need to require celery workers.  As you add 
automations to this base webhook listener it might degrade performance.  Since IP Fabric 
discoveries are usually scheduled once or twice a day this lightweight API should be enough
to handle your needs.  An example celery project: https://github.com/GregaVrbancic/fastapi-celery

## Project status

**Code located in this repository is not actively maintained and updated.**

Using examples in this directory may require some modifications in order to get
working properly. If you would like to use one of the examples but having
difficulty running it or need assistance with updating it, please contact us
in one of the following ways:

- Reach out to the Blog Author or Video Presenter.
- Contact your Solution Architect.
- Open a [GitLab Issue](https://gitlab.com/ip-fabric/integrations/marketing-examples/-/issues)

