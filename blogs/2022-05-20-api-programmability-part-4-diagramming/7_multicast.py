from os import path

from ipfabric import IPFClient
from ipfabric.diagrams import Multicast

if __name__ == '__main__':
    ipf = IPFClient()  # IPFClient(base_url='https://demo3.ipfabric.io/', auth='token', verify=False, timeout=15)

    multicast = Multicast(
        source='10.33.230.2',
        group='233.1.1.1',
        receiver='10.33.244.200',
        protocol='tcp',
        srcPorts='1024,2048-4096',
        dstPorts='80,443',
    )

    with open(path.join('path_lookup', '7_multicast.png'), 'wb') as f:
        f.write(ipf.diagram.png(multicast))
    