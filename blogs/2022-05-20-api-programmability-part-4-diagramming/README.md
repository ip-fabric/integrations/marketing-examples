# [API Programmability - Part 4: Diagramming](https://ipfabric.io/blog/api-programmability-part-4-diagramming/)

Author: [Justin Jeffery](mailto:justin.jeffery@ipfabric.io)<br>
Published Date: 2022-05-20<br>
Updated Date: N/A

## Summary

In this blog we are using the [ipfabric](https://pypi.org/project/ipfabric/)
Python package.

## Contents

1. Host to Gateway Example
2. Network Example
   1. Site
   2. Change Layout
3. Network Overlay Examples
   1. Snapshot Compare
   2. Intent Rule
4. Download OSPF and BGP diagrams for each site
5. Unicast Example
   1. ICMP
   2. TCP
   3. Subnet Example
   4. User Defined First Hop
6. Unicast JSON vs Model
7. Multicast Example

## Project status

**Code located in this repository is not actively maintained and updated.**

Using examples in this directory may require some modifications in order to get
working properly. If you would like to use one of the examples but having
difficulty running it or need assistance with updating it, please contact us
in one of the following ways:

- Reach out to the Blog Author or Video Presenter.
- Contact your Solution Architect.
- Open a [GitLab Issue](https://gitlab.com/ip-fabric/integrations/marketing-examples/-/issues)
