from os import path

from ipfabric import IPFClient
from ipfabric.diagrams import Host2GW

if __name__ == '__main__':
    ipf = IPFClient()  # IPFClient(base_url='https://demo3.ipfabric.io/', auth='token', verify=False, timeout=15)

    # Get Random Host
    host = ipf.inventory.hosts.all(filters={"ip": ["cidr", "10.35.0.0/16"]})[3]['ip']

    with open(path.join('path_lookup', '1_host2gateway.png'), 'wb') as f:
        f.write(ipf.diagram.png(Host2GW(startingPoint=host)))
