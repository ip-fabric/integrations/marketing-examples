from ipfabric import IPFClient
from ipfabric.diagrams import Unicast, OtherOptions
from ipfabric.diagrams.output_models.graph_result import GraphResult

if __name__ == '__main__':
    ipf = IPFClient()  # IPFClient(base_url='https://demo3.ipfabric.io/', auth='token', verify=False, timeout=15)

    unicast_tcp = Unicast(
        startingPoint='10.47.117.112',
        destinationPoint='10.66.127.116',
        protocol='tcp',
        srcPorts=1025,
        dstPorts='80,443',
        otherOptions=OtherOptions(applications='http|web'),
        securedPath=True  # Must be set to True or 'passingTraffic' attribute will always return 'all'
    )

    json_data = ipf.diagram.json(unicast_tcp, snapshot_id='$last')

    model_data = ipf.diagram.model(unicast_tcp, snapshot_id='$last')

    print(GraphResult.model_json_schema())
