# [IPv4 Reclamation Using IP Fabric](https://ipfabric.io/blog/ipv4-reclamation-using-ip-fabric/)

Author: [Justin Jeffery](mailto:justin.jeffery@ipfabric.io)<br>
Published Date: 2022-02-24<br>
Updated Date: N/A

## Summary

One of the most challenging issues with a merger and acquisition, or preparing to sell your public IPv4 space, is ensuring the addresses and networks are cleaned from your environment. Using IP Fabric can remove many technical hurdles, because all your important networking information is in a single platform and accessible via a GUI and API.

I’ll be demonstrating reclaiming a 192.168.0.0/16 private space, but this methodology can be applied to any network you wish.

## Project status

**Code located in this repository is not actively maintained and updated.**

Using examples in this directory may require some modifications in order to get
working properly. If you would like to use one of the examples but having
difficulty running it or need assistance with updating it, please contact us
in one of the following ways:

- Reach out to the Blog Author or Video Presenter.
- Contact your Solution Architect.
- Open a [GitLab Issue](https://gitlab.com/ip-fabric/integrations/marketing-examples/-/issues)

