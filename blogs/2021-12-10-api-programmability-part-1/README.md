# [API Programmability - Part 1: The Basics](https://ipfabric.io/blog/api-programmability-part-1/)

Author: [Justin Jeffery](mailto:justin.jeffery@ipfabric.io)<br>
Published Date: 2021-12-10<br>
Updated Date: N/A

## Summary

This blog talked about the basics of communicating to the API using requests.
We also showed how to use the user interface to create an API key and view API documentation.

## Project status

**Code located in this repository is not actively maintained and updated.**

Using examples in this directory may require some modifications in order to get
working properly. If you would like to use one of the examples but having
difficulty running it or need assistance with updating it, please contact us
in one of the following ways:

- Reach out to the Blog Author or Video Presenter.
- Contact your Solution Architect.
- Open a [GitLab Issue](https://gitlab.com/ip-fabric/integrations/marketing-examples/-/issues)

