# integration-demos
This repo is used for the Python examples used to demonstrate IP Fabric's integrations with other systems.


This directory contains API clients for IP Fabric and other systems based on httpx, along with some demo usage scripts

* [netbox-demo](netbox-demo)

This directory contains Python Flask app and some CSS and static content to provide a webhook handler to synchronise data with Netbox as shown in the demo in [this video](https://youtu.be/EdP3v8fq_jo)


## Project status

**Code located in this repository is not actively maintained and updated.**

Using examples in this directory may require some modifications in order to get
working properly. If you would like to use one of the examples but having
difficulty running it or need assistance with updating it, please contact us
in one of the following ways:

- Reach out to the Blog Author or Video Presenter.
- Contact your Solution Architect.
- Open a [GitLab Issue](https://gitlab.com/ip-fabric/integrations/marketing-examples/-/issues)
