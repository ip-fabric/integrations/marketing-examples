# [#TF1 - Using the IP Fabric Ansible Collection to Perform Auto-remediation](https://www.linkedin.com/events/tf1-usingtheipfabricansiblecoll7021443060131622912)

Author: [Alex Gittings](mailto:alexander.gittings@ipfabric.io)<br>
Published Date: 2023-01-23<br>
Updated Date: N/A

## Summary

IP Fabric's Ansible Collection enables network professionals to get the information they need into one of the most popular network automation frameworks available.

IP Fabric's Alex Gittings is joined by Red Hat's Martin Moucka for a demonstration of exactly how to leverage your IP Fabric data for automation projects.

## Project status

**Code located in this repository is not actively maintained and updated.**

Using examples in this directory may require some modifications in order to get
working properly. If you would like to use one of the examples but having
difficulty running it or need assistance with updating it, please contact us
in one of the following ways:

- Reach out to the Blog Author or Video Presenter.
- Contact your Solution Architect.
- Open a [GitLab Issue](https://gitlab.com/ip-fabric/integrations/marketing-examples/-/issues)
