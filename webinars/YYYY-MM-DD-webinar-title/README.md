# [Webinar Title/Summary](Webinar Link)

Author: [Solution Architecture](mailto:solution.architecture@ipfabric.io)<br>
Published Date: < DATE ><br>
Updated Date: N/A

## Summary

< Summary >

## Project status

**Code located in this repository is not actively maintained and updated.**

Using examples in this directory may require some modifications in order to get
working properly. If you would like to use one of the examples but having
difficulty running it or need assistance with updating it, please contact us
in one of the following ways:

- Reach out to the Blog Author or Video Presenter.
- Contact your Solution Architect.
- Open a [GitLab Issue](https://gitlab.com/ip-fabric/integrations/marketing-examples/-/issues)
