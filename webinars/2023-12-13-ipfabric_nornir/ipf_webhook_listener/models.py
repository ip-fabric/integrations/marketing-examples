from datetime import datetime
from typing import Optional, Union

from pydantic import BaseModel, Field


class Snapshot(BaseModel):
    snapshot_id: str = Field(alias="id")
    name: Optional[str] = None
    clone_id: Optional[str] = Field(None, alias="cloneId")
    file: Optional[str] = None


class Event(BaseModel):
    type: str
    action: str
    status: str
    test: Optional[bool] = False
    requester: str
    snapshot: Optional[Snapshot] = None
    timestamp: datetime
    report_id: Optional[Union[str, list]] = Field(None, alias="reportId")
    snapshot_id: Optional[str] = Field(None, alias="snapshotId")
