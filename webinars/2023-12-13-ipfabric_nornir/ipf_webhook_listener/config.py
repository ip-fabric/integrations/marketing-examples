from pydantic_settings import BaseSettings
from typing import Optional


class Settings(BaseSettings):
    ipf_secret: str
    ipf_url: str
    ipf_token: str
    net_user: str
    net_pass: str
    net_enable: Optional[str] = None
    ipf_verify: bool = True
    ipf_test: bool = False

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
