from .models import Event
from .config import settings
from ipfabric import IPFClient

from nornir import InitNornir
from nornir.core import Nornir
from nornir_netmiko.tasks import netmiko_send_config, netmiko_save_config
from collections import defaultdict
from nornir_utils.plugins.functions import print_result


def save_config(nr: Nornir, ipf: IPFClient = None, hosts=None):
    if not hosts:
        sns = [
            d["sn"]
            for d in ipf.technology.management.saved_config_consistency.all(
                columns=["sn"], filters={"status": ["eq", "changed"]}
            )
        ]
        hosts = nr.filter(filter_func=lambda h: h.data["sn"] in sns)
        print(f'\n\nDevices with unsaved configs: {hosts.inventory.hosts.keys()}')
    save = hosts.run(task=netmiko_save_config)
    print("Save Configs.")
    print_result(save)


def run_command(hosts, commands: list, nr: Nornir):
    results = hosts.run(
        task=netmiko_send_config,
        config_commands=commands,
    )
    print_result(results)
    save_config(nr, hosts=hosts)


def check_snmp(ipf: IPFClient, nr: Nornir):
    snmp = defaultdict(list)
    for community in ipf.technology.management.snmp_communities.all(
        filters={"name": ["ieq", "public"]}
    ):
        snmp[community["name"]].append(community)

    print("\n\nDisable SNMP public community.")
    for community, data in snmp.items():
        sns = [d["sn"] for d in data]
        hosts = nr.filter(filter_func=lambda h: h.data["sn"] in sns)
        run_command(hosts, [f"no snmp-server community {community}"], nr)


def check_telnet(ipf: IPFClient, nr: Nornir):
    lines = defaultdict(list)
    for line in ipf.technology.management.aaa_lines.all(
        filters={"inTransports": ["any", "eq", "telnet"]}
    ):
        lines[f'line {line["type"]} {line["idList"].replace("-", " ")}'].append(line)

    print("\n\nDisable Telnet.")
    for line, data in lines.items():
        sns = [d["sn"] for d in data]
        hosts = nr.filter(filter_func=lambda h: h.data["sn"] in sns)
        run_command(hosts, [line, "transport input ssh"], nr)


def process_event(event: Event):
    if event.type == "intent-verification" and (
        event.test
        or (event.status == "completed" and event.requester == "snapshot:discover")
    ):
        snapshot_id = event.snapshot_id if not event.test else "$last"
        ipf = IPFClient(
            settings.ipf_url,
            auth=settings.ipf_token,
            verify=settings.ipf_verify,
            timeout=20,
            snapshot_id=snapshot_id,
        )

        # DEMO PURPOSES:
        ipf.attribute_filters = {"siteName": ["35PRODUCTION"]}

        nr = InitNornir(
            inventory={
                "plugin": "IPFabricInventory",
                "options": {
                    "base_url": settings.ipf_url,
                    "token": settings.ipf_token,
                    "verify": settings.ipf_verify,
                    "platform_map": "netmiko",
                    "snapshot_id": snapshot_id,
                    "connection_options": {
                        "netmiko": {
                            "username": settings.net_user,
                            "password": settings.net_pass,
                            "extras": {
                                "ssh_config_file": "/etc/ssh/ssh_config",
                                "secret": settings.net_enable or settings.net_pass,
                            },
                        },
                    },
                },
            }
        )

        save_config(nr, ipf)
        check_telnet(ipf, nr)
        check_snmp(ipf, nr)

        # DEMO PURPOSES TO REVERT CHANGES:
        # hosts = nr.filter(sn='69234702')
        # hosts.run(
        #     task=netmiko_send_config, config_commands=["line vty 0 4", "transport input telnet ssh"]
        # )
        # hosts.run(
        #     task=netmiko_send_config, config_commands=["snmp-server community PUBLIC RO"]
        # )
        # hosts.run(task=netmiko_save_config)
        # nr.filter(sn='69232653').run(
        #     task=netmiko_send_config, config_commands=["snmp-server community public RO"]
        # )
        # nr.filter(sn='69232653').run(
        #     task=netmiko_send_config, config_commands=["line vty 0 2", "transport input telnet ssh"]
        # )
