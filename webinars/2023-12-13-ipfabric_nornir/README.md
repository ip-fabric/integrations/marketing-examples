# [Using IP Fabric with Nornir](https://www.youtube.com/live/XOIC11eVREU?si=jariuhv8KFjbyjwA)

Author: [Solution Architecture](mailto:solution.architecture@ipfabric.io)<br>
Published Date: 2023-12-13<br>
Updated Date: N/A

## Summary

This demo uses the following to automate the remediation of a network configurations:

- [IP Fabric Python SDK](https://ipfabric.io/developers/python-sdk/)
  - [IP Fabric SDK Blog Post](https://ipfabric.io/blog/api-programmability-python/)
- [IP Fabric Webhook Blog Post](https://ipfabric.io/blog/api-programmability-part-3-webhooks/)
- [Nornir](https://nornir.readthedocs.io/en/latest/)
- [ipfabric_nornir Plugin](https://pypi.org/project/ipfabric_nornir/)
  - [ipfabric_nornir Repository](https://gitlab.com/ip-fabric/integrations/ipfabric_nornir)

## Installation

```bash
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
cp sample.env .env
```

Edit `.env` file and fill in the required values.

## Running

`uvicorn ipf_webhook_listener.api:app --host <IP_ADDRESS>`

## Project status

**Code located in this repository is not actively maintained and updated.**

Using examples in this directory may require some modifications in order to get
working properly. If you would like to use one of the examples but having
difficulty running it or need assistance with updating it, please contact us
in one of the following ways:

- Reach out to the Blog Author or Video Presenter.
- Contact your Solution Architect.
- Open a [GitLab Issue](https://gitlab.com/ip-fabric/integrations/marketing-examples/-/issues)
